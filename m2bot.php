<?php

/**
 * Copyright 2016 LINE Corporation
 * Copyright 2018 Stanley <angstanley68@gmail.com>
 *  
 * This file is part of M2RemedBot.
 * 
 * M2RemedBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * M2RemedBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with M2RemedBot.  If not, see <https://www.gnu.org/licenses/>.
 */

require_once('./LINEBotTiny.php');
date_default_timezone_set("Asia/Jakarta");

$channelAccessToken = 'masukkan_token';
$channelSecret = 'masukkan_secret';
	
$anoturl = 'https://i.postimg.cc/JrmjP6rw/anot.png?dl=1';

$client = new LINEBotTiny($channelAccessToken, $channelSecret);
foreach ($client->parseEvents() as $event) {
	switch ($event['type']) {
		case 'message':
			$message = $event['message'];
			switch ($message['type']) {
				case 'text':
					if (strpos($message['text'], '/remed') !== false) {
						$message['text'] =  ltrim($message['text'],"/remed ");
					} else {
						die();
					}
					
					
					//$tokenurl = "https://bancet.cf/m2bot/getdata.php?gettoken=1";
					//$token = json_decode(file_get_contents($tokenurl));
					//$auth = hash($token->{'token'},'qwerty1234');
					$auth = md5(date("dmyHi"));
					
					$cekurl = "https://bancet.cf/m2bot/getdata.php?auth=".$auth."&nis=".$message['text'];
					$data = json_decode(file_get_contents($cekurl));
					
					if (array_key_exists('error', $data)) {
						$output = $data->{'error'};
					} else {
						$output = "Nama: ".$data->{'Nama'}." (".$data->{'NIS'}.")\nKelas: ".$data->{'Kelas'}."\n\nDaftar Remedial:";
						
						$remedcount = 0;
						$inprocesscount = 0;
						while($element = current($data)) {
							if ($element == 'Remed') {
								$output = $output ."\n- " . key($data);
								$remedcount++;
							} else if ($element == 'In Process') {
								//$output = $output ."\n- " . key($data);
								$inprocesscount++;
							}
							next($data);
						}
						reset($data);
						
						
						if ($remedcount == 0) {
							$output = $output . "\nTidak ada";
						} else {
							$output = $output . "\n\nTotal remedial: ". $remedcount;
						}
						
						if ($inprocesscount > 0) {
							$output = $output . "\n\nSedang Diproses: ";
							$inprocesscount = 0;
						}
						
						while($element = current($data)) {
							if ($element == 'In Process') {
								$output = $output ."\n- " . key($data);
								$inprocesscount++;
							}
							next($data);
						}
						reset($data);
						
						/**
						if ($inprocesscount !== 0) {
							$output = $output . "\n\nJangan senang dulu, masih proses hehhe...";
						} else {
							if ($remedcount == 0) {
								$output = $output . "\n\nHebat ya, nyontek kan? hayo...";
							} else if ($remedcount == 6) {
								$output = $output . "\n\nAnjir gila kau remed semua :v";
							} else if ($remedcount >= 3 && $remedcount <= 5) {
								$output = $output . "\n\nMantap kali kau nak, mamak kau pasti bangga.";
							} else if ($remedcount >= 1 && $remedcount <= 2) {
								$output = $output . "\n\nMampos remed kau wkwkwk";
							}
						}
						*/
						
					}
					
					$outputmessage = [];
					
					if ($message['text'] == '25747') {
						$outputmessage[] = ['type' => 'image',
											'originalContentUrl' => $anoturl,
											'previewImageUrl' => $anoturl];
					}
					
					$outputmessage[] = ['type' => 'text', 'text' => $output];
					
					$client->replyMessage([
						'replyToken' => $event['replyToken'],
						'messages' => $outputmessage
					]);
					break;
				default:
					error_log('Unsupported message type: ' . $message['type']);
					break;
			}
			break;
		default:
			error_log('Unsupported event type: ' . $event['type']);
			break;
	}
};
