<?php
/**
 * Copyright 2018 Stanley <angstanley68@gmail.com>
 * 
 * This file is part of M2RemedBot.
 * 
 * M2RemedBot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * M2RemedBot is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with M2RemedBot.  If not, see <https://www.gnu.org/licenses/>.
 */

error_reporting(E_ERROR | E_PARSE);
date_default_timezone_set("Asia/Jakarta");

$gettoken = $_GET["gettoken"];

if ($gettoken == "1") {
    //echo json_encode(array("token" => md5(date("dmyHi"))));
    echo json_encode(array(
        "error" => "gettoken not allowed"
    ));
    die();
}

$nis      = $_GET["nis"];
$auth     = $_GET["auth"];
$auth_gen = md5(date("dmyHi"));

// authentication
if ($auth !== $auth_gen) {
    echo json_encode(array(
        "error" => "Authentication failed"
    ));
    die();
}

// cek apakah NIS sudah diinput
if ($nis == "") {
    echo json_encode(array(
        "error" => "Cara penggunaan: /remed NIS_anda\n\nContoh: /remed 12345"
    ));
    die();
}

// lakukan HTTP Post ke server
$url  = 'http://methodist2mdn.sch.id/daftar-remedial/';
$data = array(
    'iptsearch' => $nis,
    'cmdok' => 'Periksa'
);

// use key 'http' even if you send the request to https://...
$options     = array(
    'http' => array(
        'header' => "Content-type: application/x-www-form-urlencoded\r\n",
        'method' => 'POST',
        'content' => http_build_query($data)
    )
);
$context     = stream_context_create($options);
$htmlContent = file_get_contents($url, false, $context);

// cek jika terjadi error
if ($htmlContent === FALSE) {
    echo json_encode(array(
        "error" => "Unknown error"
    ));
    die();
}

// cek apakah NIS terdaftar
if (strpos($htmlContent, 'NIS anda belum terdaftar sebagai peserta ujian.') !== false) {
    echo json_encode(array(
        "error" => "NIS anda belum terdaftar sebagai peserta ujian."
    ));
    die();
}

// load HTML dengan DOMDocument
$DOM = new DOMDocument();
$DOM->loadHTML($htmlContent);

// cari table dengan tag <th> dan <td>
$Header = $DOM->getElementsByTagName('th');
$Detail = $DOM->getElementsByTagName('td');

$datauser = array();

// looping sampai ketemu null
for ($x = 0; $Detail->item($x)->nodeValue !== null; $x++) {
    // masukkan data ke array
    $datauser += array(
        $Header->item($x)->nodeValue => $Detail->item($x)->nodeValue
    );
}

// output data dengan json
echo json_encode($datauser);
?>